# OpenClassroom - Python

### Author:
Fabien HUITELEC

### Description:
Practical works realized as part of a Python (3.4) certificating course delivered by OpenClassroom.<br />
To acknowledge the content of the said PW, please click the link below.<br />
Comments, messages and var names are still in French. Dear english speakers, forgive me.<br />

[Visit OpenClassroom Python main page](http://openclassrooms.com/courses/apprenez-a-programmer-en-python)
